import collections
from . import deprecated


class AbstractMixin(collections.abc.Sequence):
    @deprecated.deprecated
    def __init__(self, it):
        self._iter = it

    def __contains__(self, item):
        pass
