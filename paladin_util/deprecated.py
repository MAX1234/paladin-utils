import sys
import typing


class DeprecationError(RuntimeError):
    pass


def deprecated(f: typing.Callable):
    def inner(*args, **kwargs):
        sys.stderr.write('Deprecation warning: %s' % f.__name__)
        return f(*args, **kwargs)

    return inner


def error(f: typing.Callable):
    def inner():
        raise DeprecationError('Deprecation warning: %s' % f.__name__)

    return inner
