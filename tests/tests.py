# Copyright (C) 2019 Max Steinberg

import unittest
from paladin_util import (namespace, complexrange, abstractmixin, deprecated)
import sys


class NamespaceTests(unittest.TestCase):
    def test_namespace_simple(self):
        nam = namespace.Namespace({
            'data': 'a'
        })

        self.assertEqual(nam.data, 'a')

    def test_set(self):
        nam = namespace.Namespace({
            'data': 'a'
        })

        self.assertEqual(nam.data, 'a')
        nam.data = 'b'
        self.assertEqual(nam.data, 'b')


class ComplexRangeTest(unittest.TestCase):
    def test_complex_range_simple(self):
        for i in complexrange.complexrange((1, 2), (1, 2)):
            self.assertTrue(i in [1 + 1j, 2 + 2j])

    def test_throws(self):
        self.assertRaises(ValueError, complexrange.complexrange, (1, 2), (1, 100))

    def test_negative_step(self):
        for i in complexrange.complexrange((32, 1, -1), (1, 32)):
            self.assertEqual(i.real + i.imag, 33)


# class AbstractMixinTest(unittest.TestCase):
#     def test_abstract_mixin(self):
#         mix = abstractmixin.AbstractMixin(x for x in range(10))
#         mix.chain(lambda x: x * 2)
#
#         n = 0
#         for i in mix:
#             self.assertEqual(2 * n, i)
#             n += 1


class DeprecatedTest(unittest.TestCase):
    def test_deprecated(self):
        @deprecated.deprecated
        def f():
            pass

        f()

    def test_error(self):
        @deprecated.error
        def f():
            pass

        self.assertRaises(deprecated.DeprecationError, f)

